<?php

namespace App\Http\Controllers\api;

use http\Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function Login(Request $request)
    {

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            $user = Auth::user();
            return [
                'email' => $user->email,
                'name' => $user->name,
                'api_token' => $user->api_token
            ];
        }
        $exception = new AuthenticationException();

        return response()->json(['message' => $exception->getMessage()], 401);

    }
}
