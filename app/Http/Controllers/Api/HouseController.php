<?php

namespace App\Http\Controllers\Api;

use App\House;
use App\Http\Requests\SearchHouses;
use App\Http\Controllers\Controller;

class HouseController extends Controller
{

    /**
     * @param SearchHouses $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchHouse(SearchHouses $request)
    {
        $result = House::searchHouses($request);
        return response()->json($result);
    }

}
