<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchHouses extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'bedrooms' => 'nullable|integer|digits:1',
            'bathrooms' => 'nullable|integer|digits:1',
            'storeys' => 'nullable|integer|digits:1',
            'garages' => 'nullable|integer|digits:1',
            'min_price' => 'nullable|integer',
            'miax_price' => 'nullable|integer',
        ];
    }
}
