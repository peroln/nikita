<?php

namespace App;

use App\Http\Requests\SearchHouses;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class House extends Model
{

    /**
     * @param Request $request
     * @return mixed
     */
    public static function searchHouses(SearchHouses $request)
    {
        return House::where(function ($q) use ($request) {
            if ($request->input('name')) $q->where('name', 'LIKE', '%' . $request->input('name') . '%');
            if ($request->input('bedrooms')) $q->where('bedrooms', $request->input('bedrooms'));
            if ($request->input('bathrooms')) $q->where('bathrooms', $request->input('bathrooms'));
            if ($request->input('storeys')) $q->where('storeys', $request->input('storeys'));
            if ($request->input('garages')) $q->where('garages', $request->input('garages'));
            if ($request->input('min_price') || $request->input('max_price'))
                $q->whereBetween('price', [$request->input('min_price') ?: 0, $request->input('max_price') ?: 1000000000]);
        })->get();
    }

}
