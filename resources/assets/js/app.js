/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import store from './store/index';
import routers from './routes';

import Toolbar from './components/Toolbar';
import Vuelidate from 'vuelidate';


// index.js or main.js
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(Vuex);
Vue.use(Vuelidate);


const app = new Vue({
    el: '#app',
    store,
    data: {
        message: 'Hello Vue!'
    },
    router: routers,
    computed: {
        apiToken() {
            return this.$store.state.api_token;
        }
    },
    components: {
        appToolbar: Toolbar,
    },

});
