import VueRouter from 'vue-router';
import Register from './components/Registration';
import Login from './components/Login';
import Search from './components/Search';
import Table from './components/MyTable';

export default new VueRouter({
    routes: [
        {
            path: '/register',
            component: Register,
            name:'register'
        },
        {
            path: '/login',
            component: Login,
            name: 'login'
        },
        {
            path:'/test',
            component: Search,
            name: 'test-search',
            children: [
                {
                    path:'',
                    component: Table
                }
            ]
        },


    ],
    mode: 'history'
});