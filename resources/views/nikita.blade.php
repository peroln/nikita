<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  <script src="{{ asset('js/app.js') }}" defer></script>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
  <title>{{ config('app.name', 'Laravel') }}</title>
  {{--<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>--}}

</head>
  <body>
      <div id="app">
        <v-app>
          <app-toolbar></app-toolbar>
          <router-view></router-view>
        </v-app>

       <div v-if="apiToken">
        <app-search></app-search>
        <app-table></app-table>
       </div>
        {{--<app-login v-if="!apiToken"></app-login>--}}

      </div>
  </body>
</html>
