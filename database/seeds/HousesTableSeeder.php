<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('houses')->insert([
            ['name'=> 'The Victoria','bedrooms' => 4,'bathrooms' => 2, 'garages' => 2, 'price' => 374662,'storeys' => 2],
            ['name'=> 'The Xavier','bedrooms' => 4,'bathrooms' => 2, 'garages' => 2, 'price' => 513268,'storeys' => 1],
            ['name'=> 'The Como','bedrooms' => 4,'bathrooms' => 3, 'garages' => 3, 'price' => 454990,'storeys' => 2],
            ['name'=> 'The Aspen','bedrooms' => 4,'bathrooms' => 2, 'garages' => 2, 'price' => 384356,'storeys' => 2],
            ['name'=> 'The Lucretia','bedrooms' => 4,'bathrooms' => 3, 'garages' => 2, 'price' => 572002,'storeys' => 2],
            ['name'=> 'The Toorak','bedrooms' => 5,'bathrooms' => 2, 'garages' => 2, 'price' => 521951,'storeys' => 1],
            ['name'=> 'The Skyscape','bedrooms' => 3,'bathrooms' => 2, 'garages' => 2, 'price' => 263604,'storeys' => 2],
            ['name'=> 'The Clifton','bedrooms' => 3,'bathrooms' => 2, 'garages' => 1, 'price' => 386103,'storeys' => 1],
            ['name'=> 'The Geneva','bedrooms' => 4,'bathrooms' => 3, 'garages' => 2, 'price' => 390600,'storeys' => 2]

        ]);
    }
}
